//
//  Document.swift
//  CNUndoInspector
//
//  Created by Charles Chamblee on 8/7/17.
//  Copyright © 2017 Cocoa Niblets. All rights reserved.
//

import Cocoa

class Document: NSDocument
{
    override class func autosavesInPlace() -> Bool
    {
        return false
    }

    override func makeWindowControllers()
    {        
        let windowController = DocumentWindowController.init(windowNibName: "DocumentWindowController")
        self.addWindowController(windowController)
    }

    override func data(ofType typeName: String) throws -> Data
    {
        // Insert code here to write your document to data of the specified type. If outError != nil, ensure that you create and set an appropriate error when returning nil.
        // You can also choose to override fileWrapperOfType:error:, writeToURL:ofType:error:, or writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
        throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
    }

    override func read(from data: Data, ofType typeName: String) throws
    {
        // Insert code here to read your document from the given data of the specified type. If outError != nil, ensure that you create and set an appropriate error when returning false.
        // You can also choose to override readFromFileWrapper:ofType:error: or readFromURL:ofType:error: instead.
        // If you override either of these, you should also override -isEntireFileLoaded to return false if the contents are lazily loaded.
        throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
    }
}

