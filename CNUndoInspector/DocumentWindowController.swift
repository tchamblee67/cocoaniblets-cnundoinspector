//
//  DocumentWindowController.swift
//  CNUndoInspector
//
//  Created by Charles Chamblee on 8/7/17.
//  Copyright © 2017 Cocoa Niblets. All rights reserved.
//

import Cocoa

class DocumentWindowController: NSWindowController
{
    var numberOfFoos: Int = 0
    {
        willSet
        {
            if let undoManager = self.window?.undoManager
            {
                let oldValue = self.numberOfFoos
                
                undoManager.registerUndo(withTarget: self, handler: { (target) in
                    target.numberOfFoos = oldValue
                })
            }
        }
        
        didSet
        {
            self.updateInterface()
        }
    }
    
    @IBOutlet var undoButton: NSButton!
    @IBOutlet var redoButton: NSButton!
    @IBOutlet var numberOfFoosTextField: NSTextField!
    
    override func windowDidLoad()
    {
        super.windowDidLoad()
        
        self.updateInterface()
    }
    
    @IBAction func didClickAddFooButton(_ sender: Any)
    {
        self.numberOfFoos += 1
    }
    
    @IBAction func didClickMultiplyFoosButton(_ sender: Any)
    {
        self.numberOfFoos *= 2
    }
    
    @IBAction func didClickUndoButton(_ sender: Any)
    {
        guard let undoManager = self.window?.undoManager else
        {
            assertionFailure("Expecting undo manager")
            return
        }
        
        undoManager.undo()
    }
    
    @IBAction func didClickRedoButton(_ sender: Any)
    {
        guard let undoManager = self.window?.undoManager else
        {
            assertionFailure("Expecting undo manager")
            return
        }
        
        undoManager.redo()
    }
    
    func updateInterface()
    {
        guard let textField = self.numberOfFoosTextField else
        {
            assertionFailure("Expecting text field")
            return
        }
        
        guard let undoButton = self.undoButton else
        {
            assertionFailure("Expecting undo button")
            return
        }
        
        guard let redoButton = self.redoButton else
        {
            assertionFailure("Expecting redo button")
            return
        }
        
        textField.stringValue = String(self.numberOfFoos)
        
        if let undoManager = self.window?.undoManager
        {
            undoButton.isEnabled = undoManager.canUndo
            redoButton.isEnabled = undoManager.canRedo
        }
        else
        {
            undoButton.isEnabled = false
            redoButton.isEnabled = false
        }
    }
}
