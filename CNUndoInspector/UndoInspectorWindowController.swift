//
//  UndoInspectorWindowController.swift
//  CNUndoInspector
//
//  Created by Charles Chamblee on 8/7/17.
//  Copyright © 2017 Cocoa Niblets. All rights reserved.
//

import Cocoa

class UndoInspectorWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }
    
}
